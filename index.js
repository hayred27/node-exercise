const express = require("express");
const swapiHelper = require("./swapiHelper");
const app = express();
const PORT = 3000;
const bodyParser = require("body-parser");
const { response } = require("express");
const _ = require("lodash/core");
const { json } = require("body-parser");

app.use(express.json());

app.listen(PORT, () => console.log(`App listening on port ${PORT}!`));

app.get("/people", async (req, res, next) => {
  let page = 1;

  let peopleArray = [];

  let lastResponse = {};

  console.log(req.query);
  const getPeople = async (page) => {
    await swapiHelper
      .makeAPICall(`https://swapi.dev/api/people?page=${page}`)
      .then((response) => {
        peopleArray.push(...response.results);
        lastResponse = response;
      })
      .catch((error) => {
        res.send(error);
      });
  };
  await getPeople(page);

  for (page; lastResponse.next; page++) {
    await getPeople(page);
  }

  let displaySorted = _.sortBy(peopleArray, req.query.sortBy);

  res.json(displaySorted);
});

app.get("/planets", async (req, res) => {
  let page = 1;

  let planetArray = [];

  let lastResponse = {};

  const getPlanet = async (page) => {
    await swapiHelper
      .makeAPICall(`https://swapi.dev/api/planets?page=${page}`)
      .then((response) => {
        planetArray.push(...response.results);
        lastResponse = response;
        console.log(response);
      })
      .catch((error) => {
        res.send(error);
      });
  };
  await getPlanet(page);

  for (page; lastResponse.next; page++) {
    await getPlanet(page);
    console.log("loopIsWorking");
  }
});
