Promise.all([
  fetch("https://swapi.dev/api/planets?page=1"),
  fetch("https://swapi.dev/api/planets?page=2"),
  fetch("https://swapi.dev/api/planets?page=3"),
])
  .then((responses) => {
    return Promise.all(responses.map)((response) => {
      return response.json;
    });
  })
  .then((data) => {
    console.log(data);
  })
  .catch((err) => {
    console.log(err);
  });
