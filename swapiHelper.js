const { rejects } = require("assert");
const { builtinModules } = require("module");
const request = require("request");
const { resolve } = require("uri-js");

module.exports = {
  makeAPICall: function (url) {
    return new Promise((resolve, reject) => {
      request(url, { json: true }, (err, res, body) => {
        if (err) rejects(err);
        resolve(body);
      });
    });
  },
};
